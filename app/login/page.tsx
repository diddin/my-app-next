import Image from 'next/image'
import styles from './page.module.css'
import Form from '../components/Form'

export default function Page() {
  return (
    <main className={styles.main}>
      <Form/>
    </main>
  )
}
